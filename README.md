# Populate App
This is a simple web app that populates a webpage with data from a JSON file.

## Demo
[Populate](https://populate-one.vercel.app/)

## Getting Started
To view the app, simply open the `index.html` file in a web browser. The app fetches data from a `data.json` file and displays it dynamically on the webpage.

### Prerequisites
- A modern web browser

## Usage
1. Open the `index.html` file in your preferred web browser.
2. The app will fetch data from `data.json` and display it in a categorized format.

## Files and Structure
- `index.html`: The main HTML file containing the structure of the webpage.
- `styles.css`: The CSS file for styling the webpage.
- `script.js`: The JavaScript file responsible for fetching data and dynamically populating the webpage.

## Data Format
The app uses data in the following format:

```json
[
  {
    "category": "Men",
    "total": 212.4,
    "country": {
      "australia": 12.84,
      "indonesia": 139.13,
      "ghana": 16.46,
      "kenya": 27.32
    }
  },
  // ... other data entries
]
