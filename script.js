document.addEventListener("DOMContentLoaded", async function () {
  try {
    const response = await fetch('data.json');
    const jsonData = await response.json();

    displayData(jsonData);
  } catch (error) {
    console.error('Error fetching data:', error);
  }
});

function displayData(data) {
  const dataDisplay = document.getElementById("dataDisplay");

  data.forEach(item => {
    const categoryDiv = createCategoryElement(item);
    dataDisplay.appendChild(categoryDiv);
  });
}

function createCategoryElement(item) {
  const categoryDiv = document.createElement("div");
  const categoryColorClass = getCategoryColorClass(item.category);

  categoryDiv.innerHTML = `
    <div class="card-title">
      <p class="card-title__title">${item.total}M</p>
      <p class="card-title__category">${item.category}</p>
    </div>
    <div class="card">
      <p class="card-inner-title">By Country</p>
      <div class="card__inner">
        <ul class="country-list-container">
          ${Object.entries(item.country).map(([country, value]) => `
            <li class="country-list ${categoryColorClass}">
              <span class="country-list__total ${categoryColorClass}">${value}M</span>
              <span class="country-list__name ${categoryColorClass}">${country}</span>
            </li>`).join('')}
        </ul>
      </div>
    </div>`;

  return categoryDiv;
}

function getCategoryColorClass(category) {
  switch (category.toLowerCase()) {
    case 'men':
      return 'category-men';
    case 'women':
      return 'category-women';
    case 'youth':
      return 'category-youth';
    default:
      return 'category-default';
  }
}
